﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArphoxTuring
{
    static class Settings
    {
        public const string VERSION = "2016.04.14_1";

        public static byte TimeOutSecondsWhenSolving = 3;
    }
}
