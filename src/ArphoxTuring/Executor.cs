﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TuringProcessor;

namespace ArphoxTuring
{
    class Executor
    {
        CancellationTokenSource cts;
        CancellationToken ct;
        TuringProgram program;
        TuringSolver solver;

        public Executor(TuringProgram program, TuringSolver solver)
        {
            this.program = program;
            this.solver = solver;

            cts = new CancellationTokenSource();
            ct = cts.Token;
        }

        internal void Solve()
        {
            Task task = Task.Factory.StartNew(() => SolverMethod(), ct);
            if (!task.Wait(Settings.TimeOutSecondsWhenSolving * 1000, ct))
            {   //TimeOut
                cts.Cancel();
                GC.SuppressFinalize(solver); //In case of LOG in the Solver, it frees the memory allocated for big Lists of performed steps
                throw new TimeoutException(
                    string.Format("Időtúllépés, az automata nem hoz eredményt {0} másodpercen belül!", Settings.TimeOutSecondsWhenSolving)
                    );
            }
        }
        internal void PerformStep()
        {
            solver.PerformStep();
        }

        void SolverMethod()
        {
            while (!solver.Solved)
            {
                if (ct.IsCancellationRequested)
                    return;
                solver.PerformStep();
            }
        }
    }
}