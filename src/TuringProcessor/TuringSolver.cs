﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuringProcessor
{
    public class TuringSolver
    {
        const bool LOG = false;

        TuringProgram programReference;
        char[] rowHeaders;    //buffered from the TuringProgram object
        string[] colHeaders;  //buffered from the TuringProgram object
        string[,] program;    //buffered from the TuringProgram object

        public List<string> InputLines { get; private set; }
        public List<string> ReaderStates { get; private set; }
        public List<int> ReaderPositions { get; private set; }

        public string OriginalInputWord { get; private set; }
        public string InputLine { get; private set; }
        public int ReaderPosition { get; private set; }
        public string ReaderState { get; private set; }
        public bool Solved { get; private set; }


        public TuringSolver(TuringProgram program, string inputLine, int readerPosition, string readerState)
        {
            this.programReference = program;
            this.InputLine = inputLine;
            this.ReaderPosition = --readerPosition; //Cause it is 0 based in operation, 1-based on the GUI
            this.ReaderState = readerState;

            this.InputLines = new List<string>();
            this.ReaderStates = new List<string>();
            this.ReaderPositions = new List<int>();

            InputLines.Add(inputLine);
            ReaderStates.Add(ReaderState);
            ReaderPositions.Add(ReaderPosition);

            this.OriginalInputWord = inputLine;
            rowHeaders = program.GetRowSymbols;
            colHeaders = program.GetStates;
            this.program = program.GetSimplifiedProgram;

            CheckForErrors();
        }
        void CheckForErrors()
        {
            if (string.IsNullOrEmpty(InputLine))
                throw new ArgumentException(MessageStrings.InvalidInputLineError);
            if (ReaderPosition < 0 || ReaderPosition >= InputLine.Length)
                throw new ArgumentException(MessageStrings.InvalidStartingPositionError);
            if (!colHeaders.Contains(ReaderState))
                throw new ArgumentException(MessageStrings.InvalidStartingStateError);

            //If inputLine has a character which isn't in the rowHeaders AND not Symbols.EmptySymbol, then throw an Exception.
            //Validate Input Line:
            for (int i = 0; i < InputLine.Length; i++)
            {
                if (InputLine[i] != Symbols.EmptySymbol &&  //If the symbol is not the EmptySymbol AND
                    !rowHeaders.Contains(InputLine[i])      //the inputLine[i] cannot be recognized as a valid symbol
                    )
                {
                    throw new ArgumentException(MessageStrings.InvalidInputLineError);
                }
            }
        }

        public void PerformStep()
        {
            if (Solved)
                throw new ApplicationException(MessageStrings.AlreadySolvedError);

            StringBuilder sb = new StringBuilder(InputLine);

            char currentSymbol = sb[ReaderPosition];               //the current symbol to "work with"
            string currentState = ReaderState;
            int row = GetRowForSymbol(currentSymbol);              //symbol is in this row
            int col = GetColForState(currentState);                //state is in this column

            string command = program[row, col];                     //command to execute

            #region Searching for and removing Terminating symbol
            bool hasTerminatingSymbol = command.Contains(Symbols.TerminatingSymbol);
            command = command.Replace(Symbols.TerminatingSymbol.ToString(), string.Empty);
            #endregion
            //Now command doesn't have any terminating symbols.

            #region Searching for (+storing) and removing "Move" symbols
            int numOfMoveLeftSymbols = command.Count(x => x == Symbols.MoveLeftSymbol);
            command = command.Replace(Symbols.MoveLeftSymbol.ToString(), string.Empty);

            int numOfMoveRightSymbols = command.Count(x => x == Symbols.MoveRightSymbol);
            command = command.Replace(Symbols.MoveRightSymbol.ToString(), string.Empty);
            #endregion
            //Now "command" doesn't have any "Move" symbols

            string newState = GetNewState(command, currentState);   //Searching for State string
            command = command.Replace(newState, string.Empty);      //Removing state string from the command

            //Gets the new symbol to write to the input line. and writes it to the current position on the input line.
            sb[ReaderPosition] = GetNewSymbolToWrite(currentSymbol, command);

            ReaderPosition = ReaderPosition + numOfMoveRightSymbols - numOfMoveLeftSymbols; //Move the reader head, if needed

            #region Handling Out-Of-inputWord steps
            if (ReaderPosition < 0)                                 //if we want to move left to a position where there is "nothing"
            {
                ReaderPosition = 0;
                sb.Insert(0, Symbols.EmptySymbol);
            }
            else if (ReaderPosition >= InputLine.Length)
                sb.Append(Symbols.EmptySymbol);

            #endregion

            #region Saving calculated data

            ReaderState = newState;
            InputLine = sb.ToString();

            if (LOG)
            {
                InputLines.Add(InputLine);
                ReaderPositions.Add(ReaderPosition);
                ReaderStates.Add(ReaderState);
            }
            #endregion

            if (hasTerminatingSymbol)
                this.Solved = true;

            Trim(); //Trim all leading and trailing EmptySymbols if needed
        }
        public void Solve()
        {
            while (!Solved)
            {
                PerformStep();
            }
        }

        public string GetLastStepString()
        {
            return string.Format("{0}: {1}, {2}: {3}, {4}: {5}",
                "Input szó", InputLine,
                "Olvasófej pozíciója", ReaderPosition,
                "Állapot", ReaderState);
        }

        #region Inner mechanics

        /// <summary>
        /// Finds the row for the symbol given by the parameter
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        private int GetRowForSymbol(char symbol)
        {
            int i = 0;
            while (i < rowHeaders.Length && rowHeaders[i] != symbol)
                i++;

            return i;
        }

        /// <summary>
        /// Finds the column of the state given by the parameter.
        /// </summary>
        /// <param name="currentState"></param>
        /// <returns></returns>
        private int GetColForState(string currentState)
        {
            int i = 0;
            while (i < colHeaders.Length && colHeaders[i] != currentState)
                i++;

            return i;
        }

        /// <summary>
        /// Trims the InputLine. Removes all unused EmptySymbols from the beginning and the end.
        /// </summary>
        private void Trim()
        {
            StringBuilder sb = new StringBuilder(InputLine);

            //if (InputLine.Count(x => x == Symbols.EmptySymbol) > 0)
            //    Console.WriteLine();

            //Get the number of EmptySymbols:
            //1. From the start:
            int i = 0;
            while (i < InputLine.Length &&              //while we didn't get to the end AND
                InputLine[i] == Symbols.EmptySymbol &&  //the symbol is EmptySymbol AND
                ReaderPosition != i)                    //the reader isn't pointing to the current EmptySymbol
            {
                i++;
            }
            sb.Remove(0, i);
            ReaderPosition -= i;

            //2. From the end:
            int numToRemoveFromTheEnd = 0;
            i = InputLine.Length - 1;
            while (i > 0 &&                             //while we didn't get to the start AND
                InputLine[i] == Symbols.EmptySymbol &&  //the symbol is EmptySymbol AND
                ReaderPosition != i)                    //the reader isn't pointing to the current EmptySymbol
            {
                i--;
                numToRemoveFromTheEnd++;
            }
            if (numToRemoveFromTheEnd > 0)
                sb.Remove(sb.Length - numToRemoveFromTheEnd, numToRemoveFromTheEnd); //Remove the last "i" element


            InputLine = sb.ToString();
        }

        /// <summary>
        /// Gets the new state based on the given command string and the current state.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="currentState"></param>
        /// <returns></returns>
        private string GetNewState(string command, string currentState)
        {
            string newState = currentState;

            //Search for the next state from the "command" string
            for (int i = 0; i < colHeaders.Length; i++)
                if (command.Contains(colHeaders[i]))
                    newState = colHeaders[i];

            return newState;
        }

        /// <summary>
        /// If the command contains a new symbol to write on the line, returns with it.
        /// Otherwise, returns the current symbol.
        /// </summary>
        /// <param name="currentSymbol"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        private char GetNewSymbolToWrite(char currentSymbol, string command)
        {
            //Command can has 0 or 1 new symbol.
            char newSymbol = currentSymbol;                         //if a new symbol isn't found, the currentSymbol will be used.

            int i = 0;
            while (i < rowHeaders.Length && !command.Contains(rowHeaders[i]))
                i++;

            if (i < rowHeaders.Length)
            {
                newSymbol = rowHeaders[i];
                command = command.Replace(newSymbol.ToString(), string.Empty);
            }

            return newSymbol;
        }

        #endregion
    }
}