﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuringProcessor
{
    //Code-check ✔
    public static class Symbols
    {
        public const char ReaderPosSymbol = '↑';

        public const char TerminatingSymbol = '!';
        public const char EmptySymbol = 'λ';
        public const char StayInPlaceSymbol = 'H';
        public const char MoveLeftSymbol = 'B';
        public const char MoveRightSymbol = 'J';
    }
}
