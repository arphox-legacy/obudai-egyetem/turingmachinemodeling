﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuringProcessor
{
    //The operations are always performed on the Simplified program, because it is always available (given or converted into)
    public enum TuringProgramType { Simplified, Extended }
    public class TuringProgram
    {
        string[,] programExtended;
        string[,] programSimplified;
        char[] rowSymbols;              //e.g.: +, -, |, *, =
        string[] states;                //e.g.: q0, q1, q2, q3

        //Public Properties
        /// <summary>
        /// Returns a copy of the Simplified program string[,]. Read-only.
        /// </summary>
        public string[,] GetSimplifiedProgram
        {
            get { return CopyArray(programSimplified); }
        }

        /// <summary>
        /// Returns a copy of the Extended program string[,]. Read-only.
        /// </summary>
        public string[,] GetExtendedProgram
        {
            get
            {
                if (!HasExtended)
                    throw new NullReferenceException(MessageStrings.NoExtendedProgramSpecified);

                return CopyArray(programExtended);
            }
        }

        /// <summary>
        /// Returns a copy of the RowSymbol characters (symbols found as each row's first element).
        /// </summary>
        public char[] GetRowSymbols
        {
            get { return CopyArray(rowSymbols); }
        }

        /// <summary>
        /// Returns a copy of the possible states in the program ("column headers")
        /// </summary>
        public string[] GetStates
        {
            get { return CopyArray(states); }
        }

        /// <summary>
        /// Gets the boolean value that represents whether the program has an extended program, too; or not.
        /// </summary>
        public bool HasExtended
        {
            get { return programExtended != null; }
        }

        //Ctor
        /// <summary>
        /// Creats a new turing program representation.
        /// Also generates the simplified version of the pgoram, if an extended program is given.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isExtended"></param>
        public TuringProgram(string path, bool isExtended)
        {
            LoadProgram(path, isExtended);
            LoadHeaders();
        }
        public TuringProgram(string[,] simplifiedProgram)
        {
            programSimplified = simplifiedProgram;
            programExtended = null;
            LoadHeaders();
        }

        /// <summary>
        /// Loads the program from the path.
        /// Also generates the simplified program if needed.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isExtended"></param>
        void LoadProgram(string path, bool isExtended)
        {
            if (isExtended)
            {
                programExtended = FileOperator.ReadFile(path);
                programSimplified = ExtendedToSimplified(programExtended);
            }
            else
            {
                programSimplified = FileOperator.ReadFile(path);
                programExtended = null;
            }
        }

        /// <summary>
        /// Loads the rowSymbols and states from the simplified program.
        /// </summary>
        void LoadHeaders()
        {
            int numOfRows = programSimplified.GetLength(0);
            int numOfCols = programSimplified.GetLength(1);

            rowSymbols = new char[numOfRows];
            states = new string[numOfCols];

            for (int i = 0; i < numOfRows; i++)
                rowSymbols[i] = programSimplified[i, 0][0];

            for (int j = 0; j < numOfCols; j++)
                states[j] = programSimplified[0, j];
        }

        //Output:

        /// <summary>
        /// Writes the program to a file.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="path"></param>
        public void WriteProgramToFile(TuringProgramType type, string path)
        {
            if (type == TuringProgramType.Extended && HasExtended)
                FileOperator.WriteFile(programExtended, path);
            else if (type == TuringProgramType.Simplified)
                FileOperator.WriteFile(programSimplified, path);
            else
                throw new NullReferenceException("No such 'program'!");
        }

        #region Static

        #region ExtendedToNormal

        /// <summary>
        /// Converts an Extended program to a Simplified program.
        /// </summary>
        /// <param name="program"></param>
        /// <returns></returns>
        static string[,] ExtendedToSimplified(string[,] program)
        {
            int xLength = program.GetLength(0);
            int yLength = program.GetLength(1);
            string[,] output = new string[xLength, yLength];

            Array.Copy(program, output, program.Length);

            output = Simplifier4thCondition(output);
            xLength = output.GetLength(0);
            yLength = output.GetLength(1);

            //Modify the middle:
            for (int i = 1; i < xLength; i++)
                for (int j = 1; j < yLength; j++)
                    output[i, j] = Remover(output[i, j], Convert.ToChar(output[i, 0]), output[0, j]);

            return output;
        }
        //Uses the following methods:

        /// <summary>
        /// Filters the string[,] by the 4th condition given in the ppt
        /// </summary>
        /// <param name="program"></param>
        /// <returns></returns>
        static string[,] Simplifier4thCondition(string[,] program)
        {
            /*
            4. ha egy oszlopban minden állapot azonos a „tetejével”, minden jel
            azonos „sorelejével” és minden mozgás „H”, akkor ezen jelet a séma
            többi részében !jellel jelöljök, és oszlopot kihagyjuk              */

            int numOfRows = program.GetLength(0);
            int numOfCols = program.GetLength(1);
            int j;
            int i;
            #region Is there a special column?
            for (i = 1; i < numOfCols; i++)
            {
                int numOfSpecialElements = 0; //count how many special elements meet the conditions

                for (j = 1; j < numOfRows; j++)
                {
                    string rowHeader = program[j, 0];
                    string colHeader = program[0, i];

                    if (program[j, i].Contains(Symbols.StayInPlaceSymbol) && //the move is "stay in place"
                        program[j, i].Contains(rowHeader) &&
                        program[j, i].Contains(colHeader))
                    {
                        numOfSpecialElements++;
                    }
                }

                if (numOfSpecialElements == numOfRows - 1)
                    break;
            }
            #endregion

            #region If there is a special column...
            if (i < numOfCols) //Akkor van speciális oszlop. j tárolja a speciális oszlop indexét.
            {
                string specialState = program[0, i];
                int specialColIndex = i;

                //Itt kéne azokat a speciális teendőket megcsinálni, hogy:
                //1. Minden, ami az adott sor állapota, azt ! jelre cseréljük (Symbols.TerminatingChar)

                #region Overwrite every special state to TerminatingChar
                for (j = 1; j < numOfRows; j++)
                    for (i = 1; i < numOfCols; i++)
                        if (program[j, i].Contains(specialState))
                            program[j, i] = program[j, i].Replace(specialState, Symbols.TerminatingSymbol.ToString());
                #endregion

                #region Remove the unnecessary column
                program = RemoveCol(specialColIndex, program);
                #endregion
            }
            #endregion

            return program;
        }

        /// <summary>
        /// Removes unnecessary symbols from the program's given cell (element)
        /// </summary>
        /// <param name="element"></param>
        /// <param name="rowHeader"></param>
        /// <param name="colHeader"></param>
        /// <returns></returns>
        static string Remover(string element, char rowHeader, string colHeader)
        {
            //Remove all StayInPlace symbols
            element = element.Replace(Symbols.StayInPlaceSymbol.ToString(), string.Empty);

            //In the given row, the row's header is the default
            element = element.TrimStart(new char[] { rowHeader });

            //In the given column, the column's header (state) is the default
            element = element.Replace(colHeader, "");

            return element;
        }

        /// <summary>
        /// Removes the input 2-dimensional matrix's specified column given by the index.
        /// </summary>
        /// <param name="colIndex"></param>
        /// <param name="inputData"></param>
        /// <returns></returns>
        static string[,] RemoveCol(int colIndex, string[,] inputData)
        {
            //Készítette: Lőrincz Attila @2016.03.04, péntek, 23:54
            //Köszi!
            for (int i = 0; i < inputData.GetLength(0); i++)
                for (int j = colIndex; j < inputData.GetLength(1) - 1; j++)
                    inputData[i, j] = inputData[i, j + 1];

            string[,] newArray = new string[inputData.GetLength(0), inputData.GetLength(1) - 1];

            for (int i = 0; i < newArray.GetLength(0); i++)
                for (int j = 0; j < newArray.GetLength(1); j++)
                    newArray[i, j] = inputData[i, j];

            return newArray;
        }

        #endregion

        #region Copy methods

        /// <summary>
        /// Returns with a shallow copy of the given 2-dimensional array.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        static T[,] CopyArray<T>(T[,] input)
        {
            if (input == null)
                throw new ArgumentNullException("Input array cannot be null!");

            T[,] output = new T[input.GetLength(0), input.GetLength(1)];
            Array.Copy(input, output, input.Length);
            return output;
        }
        /// <summary>
        /// Returns with a shallow copy of the given 1-dimensional array.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        static T[] CopyArray<T>(T[] input)
        {
            if (input == null)
                throw new ArgumentNullException("Input array cannot be null!");

            T[] output = new T[input.Length];
            Array.Copy(input, output, input.Length);
            return output;
        }

        #endregion
        #endregion
    }
}