﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuringProcessor
{
    public static class MessageStrings
    {
        public const string InvalidInputLineError = "Az input szónak helyesnek kell lennie!";
        public const string InvalidStartingPositionError = "A kezdő pozíciónak helyesnek kell lennie!";
        public const string InvalidStartingStateError = "A kezdő állapotnak helyesnek kell lennie!";

        public const string AlreadySolvedError = "Már meg van oldva!";

        public const string NoExtendedProgramSpecified = "This object doesn't have Extended program specified!";
    }
}
